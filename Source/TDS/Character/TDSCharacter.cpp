// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/World.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = HeightCamera;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();
// 	FInputModeGameOnly InputMode;
// 	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetInputMode(InputMode);
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	InputMouseWheel(DeltaSeconds);
	MovementTick(DeltaSeconds);
	RotateCharMouse(DeltaSeconds);
	
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* MyInputComponent)
{
	Super::SetupPlayerInputComponent(MyInputComponent);
	MyInputComponent->BindAxis(TEXT("MoveForward"),this,&ATDSCharacter::InputAxisX);
	MyInputComponent->BindAxis(TEXT("MoveRight"),this,&ATDSCharacter::InputAxisY);
	MyInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATDSCharacter::CameraZoom);
}

/** Rotation character */
void ATDSCharacter::RotateCharMouse(float DeltaTime)
{
 	if (IsRun==false)
 	{
		FVector2D ViewportSize = FVector2D(GEngine->GameViewport->Viewport->GetSizeXY());
		FVector2D PosMouse;
		GetWorld()->GetGameViewport()->GetMousePosition(PosMouse);

		if (PosMouse.X >= ViewportSize.X / 2)
		{
			if (PosMouse.Y < ViewportSize.Y / 2)
			{
				FindRotaterResultYaw = FMath::RadiansToDegrees(UKismetMathLibrary::Atan2(FMath::DegreesToRadians(PosMouse.X - (ViewportSize.X / 2)), FMath::DegreesToRadians((ViewportSize.Y / 2 - PosMouse.Y))));
			}
			else
			{
				FindRotaterResultYaw = FMath::RadiansToDegrees(UKismetMathLibrary::Atan2(FMath::DegreesToRadians(PosMouse.Y - ViewportSize.Y / 2), FMath::DegreesToRadians(PosMouse.X - (ViewportSize.X / 2)))) + 90;

			}
		}
		else
		{
			if (PosMouse.Y < ViewportSize.Y / 2)
			{
				FindRotaterResultYaw = -1 * FMath::RadiansToDegrees(UKismetMathLibrary::Atan2(FMath::DegreesToRadians(ViewportSize.X / 2 - PosMouse.X), FMath::DegreesToRadians(ViewportSize.Y / 2 - PosMouse.Y)));
			}
			else
			{
				FindRotaterResultYaw = -1 * FMath::RadiansToDegrees(UKismetMathLibrary::Atan2(FMath::DegreesToRadians(PosMouse.Y - ViewportSize.Y / 2), FMath::DegreesToRadians(ViewportSize.X / 2 - PosMouse.X))) - 90, 0;
			}
		}
		SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
	}
	
}

/** Zoom camera */
void ATDSCharacter::InputMouseWheel(float DeltaTime)
{
	if ((HeightCamera - (HeightMouse * SpeedZoom) > MinHeightZoom-1) && (HeightCamera - (HeightMouse * SpeedZoom) < MaxHeightZoom+1))
	{
		HeightCamera = HeightCamera - (HeightMouse * SpeedZoom);
		CameraBoom->TargetArmLength = UKismetMathLibrary::FInterpTo(CameraBoom->TargetArmLength, HeightCamera, DeltaTime, 1);
	}
}

void ATDSCharacter::CameraZoom(float Value)
{
	HeightMouse = Value;
}

/** Movement settings */
void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	if (IsRun)
	{
		if (FindRotaterResultYawMovement <90&& FindRotaterResultYawMovement >0)
		{
			if (TanForward <= 1)
			{
				AddMovementInput(FVector( 1.0f, TanForward, 0.0f), 1);
			}
			else
			{
				AddMovementInput(FVector(1/TanForward, 1.0f,  0.0f), 1);
			}
		}
		else if (FindRotaterResultYawMovement > 90)
		{
			if (TanForward <= 1)
			{
				AddMovementInput(FVector(1/TanForward, 1.0f, 0.0f), 1);
			}
			else
			{
				AddMovementInput(FVector(1.0f, 1/TanForward,  0.0f), 1);
			}
		}
		else if (FindRotaterResultYawMovement <= 0 && FindRotaterResultYawMovement > -90)
		{
			if (TanForward <= 1)
			{
				AddMovementInput(FVector(1 / TanForward, 1.0f, 0.0f), -1);
			}
			else
			{
				AddMovementInput(FVector(1.0f, 1 / TanForward, 0.0f), -1);
			}
		}
		else
		{
			if (TanForward <= 1)
			{
				AddMovementInput(FVector(1.0f, TanForward, 0.0f), -1);
			}
			else
			{
				AddMovementInput(FVector(1 / TanForward, 1.0f, 0.0f), -1);
			}
		}
	}
	else
	{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f,1.0f,0.0f),AxisY);
	}

}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimState;
		IsRun = false;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkState;
		IsRun = false;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunState;
		IsRun = false;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintRunState;
		TanForward = UKismetMathLibrary::Tan(FMath::DegreesToRadians(FindRotaterResultYaw));
		FindRotaterResultYawMovement = FindRotaterResultYaw;
		IsRun = true;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkState;
		IsRun = false;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed=ResSpeed;
}

void ATDSCharacter::ChangeMovementState(EMovementState NewMovementState)
{
	MovementState = NewMovementState;
	CharacterUpdate();
}

