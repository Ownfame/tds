// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDSCharacter.generated.h"

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDSCharacter();

	virtual void BeginPlay();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;
	
	virtual void SetupPlayerInputComponent (class UInputComponent*MyInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
	
public:

	/** Movement settings */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
		bool IsRun=false;
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void MovementTick (float DeltaTime);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NewMovementState);
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	float TanForward = 0.0f;
	float FindRotaterResultYawMovement = 0.0f;

	
	/** Scroll zoom camera */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Camera")
		float HeightCamera = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Camera")
		float MaxHeightZoom = 1200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Camera")
		float MinHeightZoom = 400.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Camera")
		float SpeedZoom = 20.0f;
	UFUNCTION()
		void InputMouseWheel(float DeltaTime);
	UFUNCTION()
		void CameraZoom (float Value);
	float HeightMouse = 0.0f;
	float FindRotaterResultYaw = 0.0f;


	/** Rotation character */
	UFUNCTION()
		void RotateCharMouse (float DeltaTime);
};

