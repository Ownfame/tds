#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	SprintRun_State UMETA(DisplayName = "Sprint Run State"),
	AimWalk_State UMETA(DisplayName = "Aim Walk State")

};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
	float AimState = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
	float WalkState = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
	float RunState = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
	float SprintRunState = 800.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Movement")
	float AimWalkState = 100.0f;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};